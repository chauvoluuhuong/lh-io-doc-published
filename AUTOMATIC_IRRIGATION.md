## Automatic irrigation application

This application is mostly applied for green houses. It make irrigating automatic control the environment based on conditions:

1.  Environment: temperature, humidity, brightness level...
2.  The scheduler that make irrigation based on timming.

Further we make configuration for irrigation follow main procedures.

- Mixing manure: we run the system to mix types of manure, water...with a certain quantity.
- Control pumps, valve to water with a timing.
- Turn of pumps in order.

Beside that, we control the curtain, fans...to make the environment in condition.

## Some of fun pictures about project implementation:

Installing sensors
![alt Installing sensors](irrigation/1.jpg)

Waiting for deployment
![alt Waiting for deployment](irrigation/2.jpg)

Over a year
![alt Over a year](irrigation/4.jpg)

Checking and maintaining after deployment
![alt Over a year](irrigation/5.jpg)

![alt Over a year](irrigation/6.jpg)

## The boards applied:

We developed multi type versions. That based on condition of using, network, demanding of I/O

LH IO V3
![alt LH IO V3](lh_boards/v3.jpeg)

The board for reading sensors and using lora (can be used with solar battery)
![alt The board for reading sensors and using lora](lh_boards/giam_sat_full_hd.jpg)

The simplest version that uses 4G network and lora
![alt The simplest version](lh_boards/z3390861834896_106a27e4342be7a21518520477ed9578.jpg)

Simplest combo
![alt The simplest version](lh_boards/IMG_20211203_110023_040.jpg)

## Web app

The web application is used for configuration, scheduling, tracking running, manage users...

### Home page

![alt Home page](irrigation/home_page.png)

## Setting watering and environmental conditioning system

![alt Setting watering and environmental conditioning system](irrigation/setting_page.png)

## Setting scheduler for watering

![alt Setting scheduler for watering](irrigation/setting_schedular.png)

## Watering history

![alt Watering history](irrigation/watering_history.png)

## Tracking environment history

![alt Watering history](irrigation/environment_data.png)

## Tracking environment history

![alt Watering history](irrigation/environment_data.png)

## User managing page

![## User managing page](irrigation/user_managing.png)
