# Boards Produced By Luu Huong Tech

## LH IO V2

- Power supply: 24v
- 8 isolated digital input 0-8v
- 8 isolated analog input 0-10V or configured to use with 4mA-20mA
- 16 isolated digital output 24V/1A
- 4 isolated DAC 0-10V
- 2 x RS232
- 1 x I2C
  ![alt LH IO V2](lh_boards/v2_full_hd.jpg)

## LH IO V3

- Power supply: 24v
- 8 isolated digital input 0-8v
- 8 isolated analog input 0-10V or configured to use with 4mA-20mA
- 12 isolated relay output 250V/1A
- 4 isolated MOSFET output 24v/1a
- 4 isolated DAC 0-10V
- 2 x RS232
- 1 x I2C
- 1 x RS485
  ![alt LH IO V3](lh_boards/v3_full_HD.jpg)

## Master board for automatic watering

- Power supply: 250v
- 10 isolated relay output 250V/5A
- Can communicate with 4g and lora

![alt Master board for automatic watering](lh_boards/z3390861834896_106a27e4342be7a21518520477ed9578.jpg)

## Tracking board

- Power supply: 12V and solar battery 18V
- 1 I2C, 2 RS485, 1 RS232, 5 x ADC
- Communicate with lora

![alt Tracking board](lh_boards/giam_sat_full_hd.jpg)
