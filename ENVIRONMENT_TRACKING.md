# Environment tracking application

This application is used to track environment by reading data from sensors such as PH sensor, humidity, temperature...
We make it flexible to work with any type of sensors, data.  
Besides getting data from sensor, we develop rule engine that help for user config the logic to fire alarms.

### The board applied

This board can be powered both from solar battery and 12V.
The reserved energy sources can help it work four days without any external power source.
![alt Board applied](lh_boards/z3390861841662_fc0c7a58396f96fb56be7b324479c017.jpg)

## Please check this video to get more detail

[![IMAGE Video youtube](http://img.youtube.com/vi/whq28MP5h34/0.jpg)](http://www.youtube.com/watch?v=whq28MP5h34 "Environment tracking application")
