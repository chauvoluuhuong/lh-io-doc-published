### Authentication / Authorization

Our backend services is mainly deployed on AWS Cloud services. And we use IAM Policy to secure and authorize using backend services
[You can refer the code example here for using IAM certificates to connect to our MQTT broker](https://docs.aws.amazon.com/iot/latest/developerguide/mqtt.html)

| Topic | Description          | Data type | Example |
| ----- | -------------------- | --------- | ------- |
|       | Get on owning boards | Json      |

updating..
