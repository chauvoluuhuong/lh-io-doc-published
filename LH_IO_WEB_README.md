# LH I/O Web app

We provide web application to make you control, manage and develop board in the easiest way

## Home page

The home page is used for connecting to the board and checking the running status of the boards.

![alt Home page](lh-io-web/LH_IO_Dashboard.png)

## Test Board page

This page make the development and testing after deployment easier

### Part 1: for reading log from board and send command to control board

![alt Testing page](lh-io-web/test_board_1.png)

### Part 2: for reading and controlling Digital/Analog

You can easily control the I/O and read status from the board
![alt Testing page 2](lh-io-web/test_board_2.png)

## Setting reading digital/analog

This page help for configuration to filter digital/analog

- Digital: you can setup filtering based on edge, width of signal
- Analog: You can setup for getting the signal whenever the last result is different from the previous result with a arbitrary number

![alt Setting reading digital/analog](lh-io-web/setting_GPIO.png)

## Setting connections

You can select for using bluetooth or MQTT. Further, you can config to connect to MQTT broker

![alt Setting connections](lh-io-web/setting_connection.png)

## Setting data logger

You can set to make the board as a trigger that will send a I2C/RS485 command periodically to get the feedback
![alt Setting data logger](lh-io-web/data_logger_page.png)

## Setting data logger

You can set to make the board as a trigger that will send a I2C/RS485 command periodically to get the feedback
![alt Setting data logger](lh-io-web/data_logger_page.png)

## Running data

This page is used for tracking the history of running boards.
![alt Running data](lh-io-web/running_data.png)
