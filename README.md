# Welcome to LH IO Document

We provide software stack to make developing for IOT project more easy to maintain, scale up

## The LH IO is used for

1.  Remote I/O
2.  Controller. Yes, it is programable. You can add more functions and replace PLC
3.  IoT gateway

## Study cases

The LH IO successfully applied to

1.  [Automatic irrigation system](AUTOMATIC_IRRIGATION.md)
2.  [UPS application](UPS.md)
3.  [Vending machine & Kiosk](VENDING_MACHINE.md)
4.  [Environment tracking](ENVIRONMENT_TRACKING.md)

Hope with its flexibilities the LH IO will offer you the next successful.

## Main components

- Firmware:
  - I/O Command
  - Library interface (Arduino based)
- Web app for configuration and manage board.
- Services for integration with another application.

## MCU (Micro controller unit) compatible

The LH I/O Firmware mainly developed on ESP32 (Wrover versions).
If you plan for apply the firmware with another MCU, please contact to us for getting supports.

you can check here for:

#### [Boards produced by Luu Huong Tech](BoardsProducedByLuuHuongTech.md)

#### Boards suggested in market: almost boards based on esp32 controller.

## LH IO Commands

Your application such as mobile app, web app can peer-to-peer (directly) or via MQTT broker send these command for controlling I/O, set up filter, connections.....

```mermaid


graph LR

A[your applications]  -->|Commands| B(The LH I/O Boards)

```

### Controlling I/O Commands

| Command name                        | Command (array of bytes) | Parameter                                        | Example                                                    | Protocol              | Note                                                                                                                                                                                                                                                               |
| ----------------------------------- | ------------------------ | ------------------------------------------------ | ---------------------------------------------------------- | --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Digital Output                      | pin : level 10           | pin: 50-57, 60-67<br />level: 1 (high), 0 (low), | make pin 1 on: 50 58 1 10 </br> make pin 1 off: 50 58 1 10 | MQTT/Serial/Bluetooth | Please take note that 58 number stands for : character, 10 is for \n <br/>The board applies feedback mechanism<br />So that whenever you send a command for setting GPIO (digital, analog) <br/> you can read the feedback as the reading commands described below |
| Read digital input                  | pin : level \n           | pin: 0-7<br/>level: 1 (high) 0 (low)             | pin digital 1 is high: 1 58 1 10                           | MQTT/Serial/Bluetooth |                                                                                                                                                                                                                                                                    |
| Read analog (voltage, 0-10v) input  | pin : value \n           | pin: 30-7<br/>0-200                              | pin analog 1 is getting 5v: 1 58 100 10                    | MQTT/Serial/Bluetooth |                                                                                                                                                                                                                                                                    |
| Read analog (current, 4-20mA) input | pin : value \n           | pin: 40-7<br/>0-160                              | pin analog 1 is getting 12mA: 1 58 80 10                   | MQTT/Serial/Bluetooth |                                                                                                                                                                                                                                                                    |
| DAC output (0-10V)                  | pin : edge_selected \n   | pin: 70-7<br/>0-160                              | pin analog 1 is getting 12mA: 1 58 80 10                   | MQTT/Serial/Bluetooth |                                                                                                                                                                                                                                                                    |

### Setting filter I/O Commands

| Command name                                 | Command (array of bytes) | Parameter                                                              | Example                                                                                             | Protocol              | Note |
| -------------------------------------------- | ------------------------ | ---------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- | --------------------- | ---- |
| filtering analog input (voltage and current) | pin : value \n           | pin: 80-7<br/>value: 0-200                                             | Get the signal whenever the last result is <br />different from the previous result 1v: 80 58 20 10 | MQTT/Serial/Bluetooth |      |
| Select edge (for digital)                    | pin : value \n           | pin: 110-7<br/>value: 0-2, 0 falling edge, 1 raising edge, 2 both edge | Get input pin 0 at falling edge : 110 58 0 10                                                       | MQTT/Serial/Bluetooth |      |

Example use I/O command with mobile phone
...we are updating

## Web app

We provide web-app to make managing, configuration and testing easily.
[get more details.](LH_IO_WEB_README.md)

## Board management services

We provide MQTT topics are used for integrating with LH IO services such as request log device, I/O history......

If you plain to use these functions with another protocol (such as web api, websocket...) please contact to us for getting supports.

[get more details.](LH_IO_SERVICES.md)

## Arduino interface Library

We provide library interface that is based on Arduino to let you interact with our stack for flexibly building your application.  
The library is used for:

1. Controlling / reading board's I/O
2. Make communicating with MQTT,bluetooth, RS485 easily.
3. Communicate with our backend's services.

![alt Arduino interface Library](lh_boards/library_interface.png)

**We don't guarantee the remaining resource.** That means, our software stack may consume RAM, CPU workload. So please make sure that your function slight, check the remaining resource carefully.

**Please contact us to get our detail policy, documentation and support**
