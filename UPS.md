# UPS application

this application is used to track the running status of the ups & solar inverter or RS485-based API.  
Get alarm when devices fail to fault condition immediately to have the right action.
Besides that, we visualize the data from devices to make it have meaning for users.

## The board applied:

This board can be powered both from solar battery and 12V.
The reserved energy sources can help it work four days without any external power source.
![alt Board applied](lh_boards/z3390861841662_fc0c7a58396f96fb56be7b324479c017.jpg)

## Web app:

### Sites page:

Firstly, we create a location for adding devices
![alt Sites page](web_projects/ups/sites_page.png)

### Devices page:

For each site (location) we have multi of devices.
![alt Devices page](web_projects/ups/ups_in_site.png)

### Parsing data:

We provide a file to config parsing data got from RS485 protocol. So that, you can apply our application with multi type of ups as long as you know about the structure of it's data
![alt Devices page](web_projects/ups/config_parsing_file.png)

### Visualize data send from device:

From the raw data we got from UPS (RS485 protocol) we parse and visualize it for user.
![alt Visualize data send from device](web_projects/ups/ups_status.png)
