# Vending machine / Kiosk application

The application is used for:

- Controlling vending machine.
- Processing digital payment with MOMO/VNpay... and cash payment with NV9/NV11
- Remotely manage and control the machines.
- Manage user, store and analyze sales.
- Update and manage items for machines.
- Providing functions to help for maintaining

Our main component:

1. Hardware/Firmware that is based on [LH IO](README.md)
2. The web app
3. Android application

## Some fun pictures about the project

The first version.
![ALT First version](vendingMachine/first_version.jpg)

The v2 version that is based on [LH IO](README.md) V2 board.
![ALT v2 version](vendingMachine/v2_version.jpg)

I am installing and fixing bug with my friend.
![ALT I am installing machine](vendingMachine/installing.jpg)
![ALT I am installing machine](vendingMachine/20201031_154408.jpg)

## The web app

### Home page

Used for showing importance and highlight information about running of machines
![ALT Home page](vendingMachine/homepage.png)

### Machine management page

For creating, activating/de-activating machine, controlling temperature...
![ALT Machine management page](vendingMachine/machine_managerment_page.png)

### Machine's items management page

For managing items in the machine
![ALT Machine's items management page](vendingMachine/items_management.png)

### Tracking transactions page

It is for showing transactions, export to excel file.
![ALT Tracking transactions page](vendingMachine/transactions.png)

## Android application:

We are updating...
